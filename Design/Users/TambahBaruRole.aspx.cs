﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Leyfi;
using Leyfi.Data.Entities;

public partial class Users_TambahBaruRole : BasePage
{
    private Users _users;

    protected void Page_Load(object sender, EventArgs e)
    {
        _users = (Users)Session["EditOpUsers"];
        FillComboElement(); 
    }

    protected void btnSimpan_Click(object sender, EventArgs e)
    {
        CreateFactory().GetUsers().InsertUsersInRoles(int.Parse(cmbRoles.SelectedValue), _users.user_id.Value);
        Response.Redirect("TambahBaru.aspx");
    }

    private void FillComboElement()
    {
        cmbRoles.DataSource = CreateFactory().GetUsers().AvailableRolesForUserID(_users.user_id.Value);
    }
}