﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBProvider;
using Npgsql;
using Leyfi.Data.Interfaces;

namespace Leyfi.Data.PostgreSQL
{
    internal class Elements : IElements
    {
         DBProvider.PostgreSQL _ps;

        public Elements(string connStr)
        {
            _ps = new DBProvider.PostgreSQL(connStr);
        }
    
        public List<Entities.Elements>  GetAll()
        {
            StringBuilder sb = new StringBuilder();
            List<Entities.Elements> result = new List<Entities.Elements>();
            sb.Append(" select ");
            sb.Append(" 	element_id,element_type  ");
            sb.Append(" from moresecure.elements  ");

            _ps.OpenConnectionAndCreateCommand();
            NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString());
            Entities.Elements na;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    na = new Entities.Elements();
                    na.element_id = int.Parse(reader["element_id"].ToString());
                    na.element_type = reader["element_type"].ToString();
                    result.Add(na);
                }
            }
            reader.Close();
            _ps.CloseConnection();

            return result;
        }
        
        public int Upsert(int? element_id, string element_type)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            if (element_id.HasValue)
            {
                sb.Append("update moresecure.elements set element_type=:valelementtype ");
                sb.Append("    where element_id = :valelementid");
            }
            else
            {
                sb.Append("insert into moresecure.elements (element_type) ");
                sb.Append(" values (:valelementtype) ");
            }

            NpgsqlParameter paramId, paramType;
            paramId = new NpgsqlParameter("valelementid", NpgsqlTypes.NpgsqlDbType.Integer);
            paramType = new NpgsqlParameter("valelementtype", NpgsqlTypes.NpgsqlDbType.Text);

            NpgsqlParameter[] Params;
            if (!element_id.HasValue)
            {
                Params = new NpgsqlParameter[] { paramType };
            }
            else {
                Params = new NpgsqlParameter[] { paramType, paramId };
                paramId.Value = element_id.Value;
            }           
            paramType.Value = element_type;

            _ps.OpenConnectionAndCreateCommand();
            returnValue = _ps.ExecuteNonQuery(sb.ToString(), Params);
            return returnValue;
        }


        public int Delete(int element_id)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append(" delete from  moresecure.elements ");
            sb.Append(" where element_id = :valelementid ");

            NpgsqlParameter paramId;
            paramId = new NpgsqlParameter("valelementid", NpgsqlTypes.NpgsqlDbType.Integer);
            NpgsqlParameter[] Params = { paramId };
            paramId.Value = element_id;

            _ps.OpenConnectionAndCreateCommand();
            returnValue = _ps.ExecuteNonQuery(sb.ToString(), Params);
            return returnValue;
        }
    }
}
