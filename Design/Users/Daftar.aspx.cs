﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Leyfi;
using Leyfi.Data.Entities;

public partial class Users_Daftar : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["EditOpUsers"] = null;
        if (!Page.IsPostBack)
        {
            GridDaftar.Rebind();
        }   
    }

     protected void GridDaftar_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            HttpCookie logged = Request.Cookies["leyfi-log"];
            if (logged.Values["name"].ToString() == ((Users)e.Item.DataItem).user_name)
            {
                return;
            }
            CreateFactory().GetUsers().Delete(((Users)e.Item.DataItem).user_id.Value);
            GridDaftar_NeedDataSource(null, null);
        }
        else if (e.CommandName == "Edit")
        {
            Session["EditOpUsers"] = ((Users)e.Item.DataItem);
            Response.Redirect("TambahBaru.aspx");
        }
        else if (e.CommandName == "InitInsert")
        {
            Response.Redirect("TambahBaru.aspx");
        }
        else if (e.CommandName == "RebindGrid")
        {
            GridDaftar.Rebind();
        }
    }

     protected void GridDaftar_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
     {
         List<Leyfi.Data.Entities.Users> users = CreateFactory().GetUsers().GetAll();
         GridDaftar.DataSource = users;
     }
}