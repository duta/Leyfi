﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default/Default.Master" AutoEventWireup="true" CodeFile="TambahBaru.aspx.cs" Inherits="Roles_TambahBaru" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server" ClientIDMode="Static">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" UpdatePanelsRenderMode="Inline">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="GridDaftar">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="GridDaftar" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
    </telerik:RadAjaxLoadingPanel>
    <div class="hero-unit">
        <div class="form-horizontal">
            <legend>
                <asp:Label ID="lblLegend" runat="server" Text="Tambah Roles"></asp:Label></legend>
            <div class="control-group">
                <label class="control-label" >Nama Roles</label>
                <div class="controls">
                    <asp:TextBox ID="txtNama" runat="server" ClientIDMode="Static"></asp:TextBox>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Description</label>
                <div class="controls">
                    <asp:TextBox ID="txtDesc" runat="server" ClientIDMode="Static"></asp:TextBox>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <label class="checkbox">
                    </label>
                    <asp:Button ID="btnSimpan" runat="server" Text="Simpan" CssClass="btn" OnClick="btnSimpan_Click" />
                </div>
            </div>
            <legend>
                <asp:Label ID="lblPermission" runat="server" Text="Permission untuk role "></asp:Label></legend>
            <telerik:RadGrid ID="GridDaftar" runat="server" GridLines="None" AllowSorting="True"
                OnNeedDataSource="GridDaftar_NeedDataSource" OnItemCommand="GridDaftar_ItemCommand"
                AllowPaging="true" ShowStatusBar="true" Skin="Black">
                <ClientSettings AllowGroupExpandCollapse="true" EnablePostBackOnRowClick="false">
                </ClientSettings>
                <MasterTableView AutoGenerateColumns="false" Width="100%" CommandItemDisplay="Top">
                    <Columns>
                        <telerik:GridBoundColumn DataField="element_id" HeaderText="" Visible="false" ReadOnly="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="element_type" HeaderText="Tipe Element">
                        </telerik:GridBoundColumn>
                        <telerik:GridButtonColumn ButtonCssClass="btn btn-danger" Text="Delete" CommandName="Delete">
                            <ItemStyle ForeColor="White" />
                        </telerik:GridButtonColumn>
                    </Columns>
                    <CommandItemSettings AddNewRecordText="Add Elements" RefreshText="Refresh" ShowRefreshButton="true"
                        ShowAddNewRecordButton="true" />
                </MasterTableView>
                <PagerStyle Mode="NextPrevAndNumeric" />
            </telerik:RadGrid>
        </div>
    </div>
    <script src="../Scripts/Roles.js"></script>
</asp:Content>

