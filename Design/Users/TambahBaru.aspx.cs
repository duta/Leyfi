﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Leyfi;
using Leyfi.Data.Entities;
using Leyfi.LetMeIn;

public partial class Users_TambahBaru : BasePage
{
    private Users _users;
    private bool edit_mode { get { return _users != null; } }
    
    protected void Page_Load(object sender, EventArgs e)
    {       
        _users = (Users)Session["EditOpUsers"];
        if (edit_mode)
        {
            lblLegend.Text = "Edit User";
            if (!IsPostBack) {
                txtNama.Text = _users.user_name;
                lblRoles.Text += txtNama.Text;
            }            
        }
        else {
            lblLegend.Text = "Tambah User";
        }
        SetVisibleDaftarRoles(edit_mode);          
    }

    protected void btnSimpan_Click(object sender, EventArgs e)
    {
        LetMeIn.Password p = new LetMeIn.Password();
        CreateFactory().GetUsers().Upsert(!edit_mode ? new int?() : _users.user_id, txtNama.Text.Trim(), p.Encrypt(txtPassword.Text.Trim()));
        if (edit_mode)
        {
            //update session
            _users.user_name = txtNama.Text.Trim();
            Session["EditOpUsers"] = _users;
            lblRoles.Text += txtNama.Text.Trim();
            HttpCookie logged = Request.Cookies["leyfi-log"];
            if (logged.Values["who"].ToString() == _users.user_id.Value.ToString())
            {
                logged.Values["name"] = txtNama.Text.Trim();
                ((Label)this.Page.Master.FindControl("lblUser")).Text = txtNama.Text.Trim();
                Response.Cookies.Set(logged);
            }
        }
        else { 
            Response.Redirect("Daftar.aspx");
        }
    }

    protected void GridDaftar_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            Roles  selected_role = ((Roles)e.Item.DataItem);
            CreateFactory().GetUsers().DeleteUsersInRoles(selected_role.role_id.Value, _users.user_id.Value );
            GridDaftar_NeedDataSource(null, null);
        }
        else if (e.CommandName == "InitInsert")
        {
            Response.Redirect("TambahBaruRole.aspx");
        }
        else if (e.CommandName == "RebindGrid")
        {
            GridDaftar.Rebind();
        }
    }

    protected void GridDaftar_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        List<Leyfi.Data.Entities.Roles> roles = new List<Roles>();
        if (edit_mode)
        {
            roles = CreateFactory().GetUsers().GetRolesByUserID(_users.user_id.Value);
        }
        GridDaftar.DataSource = roles;
    }

    private void SetVisibleDaftarRoles(bool val)
    {
        lblRoles.Visible = val;
        GridDaftar.Visible = val;
    }
}