﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Leyfi.Data.Interfaces
{
    public interface IFactory
    {
        IUsers GetUsers();
        IRoles GetRoles();
        IElements GetElements();
    }
}
