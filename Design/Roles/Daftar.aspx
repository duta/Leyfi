﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default/Default.Master" AutoEventWireup="true" CodeFile="Daftar.aspx.cs" Inherits="Roles_Daftar" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <div class="hero-unit">
        <div class="form-horizontal">
            <legend>
                <asp:Label ID="lblLegend" runat="server" Text="Daftar Roles"></asp:Label>
            </legend>
            <asp:Panel ID="PanelUsers" runat="server">
                <div class="alert alert-info" >
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <asp:Label ID="lblWarning" runat="server" Text=""></asp:Label>
                    <asp:Button ID="cmdDeleteAnyway" runat="server" Text="Lanjutkan menghapus" 
                        onclick="cmdDeleteAnyway_Click" CssClass="btn btn-danger"  />
                        <asp:HiddenField ID="HiddenFieldRoleID" runat="server" />
                </div>
            </asp:Panel>            
            
            <telerik:RadGrid ID="GridDaftar" runat="server" GridLines="None" AllowSorting="True"
                OnNeedDataSource="GridDaftar_NeedDataSource" OnItemCommand="GridDaftar_ItemCommand"
                AllowPaging="true" GroupingEnabled="true" ShowStatusBar="true" Skin="Black">
                <ClientSettings AllowGroupExpandCollapse="true" EnablePostBackOnRowClick="false" Selecting-AllowRowSelect="true">
                </ClientSettings>
                <MasterTableView AutoGenerateColumns="false" Width="100%" GroupsDefaultExpanded="false"
                    GroupLoadMode="Client" CommandItemDisplay="Top" DataKeyNames="role_id" Name="Roles">  
                    <%--<GroupByExpressions>
                        <telerik:GridGroupByExpression>
                            <SelectFields>
                                <telerik:GridGroupByField FieldName="role_name" HeaderText="Roles"  />
                                <telerik:GridGroupByField FieldName="role_description" HeaderText="Desc" />
                            </SelectFields>
                            <GroupByFields>
                                <telerik:GridGroupByField FieldName="role_id"   />
                            </GroupByFields>
                        </telerik:GridGroupByExpression>
                    </GroupByExpressions>    --%>              
                    <Columns>
                        <telerik:GridBoundColumn DataField="role_id" HeaderText="" Visible="false">
                        </telerik:GridBoundColumn>
                       <%-- <telerik:GridBoundColumn DataField="element_id" HeaderText="" Visible="false">
                        </telerik:GridBoundColumn>--%>
                        <telerik:GridBoundColumn DataField="role_name" HeaderText="Nama Role">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="role_description" HeaderText="Description">
                        </telerik:GridBoundColumn>
                       <%-- <telerik:GridBoundColumn DataField="element_type" HeaderText="Permissions">
                        </telerik:GridBoundColumn> --%> 
                        <telerik:GridButtonColumn ButtonCssClass="btn btn-link" Text="Edit" CommandName="Edit">
                        </telerik:GridButtonColumn>
                        <telerik:GridButtonColumn ButtonCssClass="btn btn-danger" Text="Delete" CommandName="Delete">
                            <ItemStyle ForeColor="White" />
                        </telerik:GridButtonColumn>
                    </Columns>
                    <CommandItemSettings AddNewRecordText="Add Roles"  RefreshText="Refresh"  ShowRefreshButton="true" ShowAddNewRecordButton="true" />
                </MasterTableView>
                <PagerStyle Mode="NextPrevAndNumeric" />
            </telerik:radgrid>
           
        
            
        </div>
    </div>
    
    <script src="../Scripts/Roles.js"></script>
</asp:Content>

