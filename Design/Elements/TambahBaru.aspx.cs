﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using Leyfi;
using Leyfi.Data.Entities;

public partial class Elements_TambahBaru : BasePage
{
    private Elements _elements;

    protected void Page_Load(object sender, EventArgs e)
    {
        lblLegend.Text = "Tambah Element";
        _elements = (Elements)Session["EditOpElements"];
        if (!IsPostBack) {
            if (Session["EditOpElements"] != null)
            {
                lblLegend.Text = "Edit Element";
                txtNama.Text = _elements.element_type;
            }
        }        
    }

   protected  void btnSimpan_Click(object sender, EventArgs e)
    {
        CreateFactory().GetElements().Upsert(_elements == null ? new int?() : _elements.element_id, txtNama.Text.Trim());      
        if (_elements != null) Response.Redirect("Daftar.aspx"); 
        txtNama.Text = ""; 
   }
}