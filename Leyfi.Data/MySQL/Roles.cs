﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using Leyfi.Data.Interfaces;

namespace Leyfi.Data.MySQL
{
    internal class Roles : IRoles
    {
        MySqlConnection _ps;

        public Roles(string connStr)
        {
            _ps = new MySqlConnection(connStr);
        }

        public List<Entities.Roles> GetAll()
        {
            StringBuilder sb = new StringBuilder();
            List<Entities.Roles> result = new List<Entities.Roles>();
            sb.Append(" select ");
            sb.Append(" 	role_id,role_name,role_description  ");
            sb.Append(" from roles  ");

            _ps.Open();

            MySqlCommand cmd = new MySqlCommand(sb.ToString(), _ps);
            var reader = cmd.ExecuteReader();
            Entities.Roles na;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    na = new Entities.Roles();
                    na.role_id = int.Parse(reader["role_id"].ToString());
                    na.role_name = reader["role_name"].ToString();
                    na.role_description = reader["role_description"].ToString();
                    result.Add(na);
                }
            }
            reader.Close();
            _ps.Close();

            return result;
        }
        
        public int Upsert(int? role_id, string role_name, string role_description)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            if (role_id.HasValue)
            {
                sb.Append("update roles set role_name= @valrolename, role_description= @valroledescription ");
                sb.Append("    where role_id = @valroleid");
            }
            else
            {
                sb.Append("insert into roles (role_name, role_description) ");
                sb.Append(" values (@valrolename, @valroledescription) ");
            }
            
            _ps.Open();

            MySqlCommand cmd = new MySqlCommand(sb.ToString(), _ps);
            if (role_id.HasValue)
            {
                cmd.Parameters.AddWithValue("@valroleid", role_id.Value);
            }
            cmd.Parameters.AddWithValue("@valrolename", role_name);
            cmd.Parameters.AddWithValue("@valroledescription", role_description);
            returnValue = cmd.ExecuteNonQuery();

            _ps.Close();
            return returnValue;
        }

        public int Delete(int role_id)
        {
            int returnValue = 0;
            StringBuilder sb;
            MySqlTransaction trans = null;
            MySqlCommand cmd = null;
            try
            {
                _ps.Open();
                
                using (trans = _ps.BeginTransaction())
                {
                    sb = new StringBuilder();
                    sb.Append("delete from  permissions ");
                    sb.Append(" where role_id = @valroleid  ");
                    cmd = new MySqlCommand(sb.ToString(), _ps, trans);
                    cmd.Parameters.AddWithValue("@valroleid", role_id);
                    returnValue += cmd.ExecuteNonQuery();

                    sb = new StringBuilder();
                    sb.Append("delete from  usersinroles ");
                    sb.Append(" where role_id = @valroleid  ");
                    cmd = new MySqlCommand(sb.ToString(), _ps, trans);
                    cmd.Parameters.AddWithValue("@valroleid", role_id);
                    returnValue += cmd.ExecuteNonQuery();

                    sb = new StringBuilder();
                    sb.Append(" delete from  roles ");
                    sb.Append(" where role_id = @valroleid ");
                    cmd = new MySqlCommand(sb.ToString(), _ps, trans);
                    cmd.Parameters.AddWithValue("@valroleid", role_id);
                    returnValue += cmd.ExecuteNonQuery();

                    trans.Commit();
                }
            }
            catch (Exception ex)
            {
                if (trans != null) trans.Rollback();
            }
            finally
            {
                _ps.Close();
            }
          
            return returnValue;
        }
        
        public List<Entities.Permissions> GetPermissions()
        {
            StringBuilder sb = new StringBuilder();
            List<Entities.Permissions> result = new List<Entities.Permissions>();
            sb.Append(QueryGetPermissions());
            _ps.Open();

            MySqlCommand cmd = new MySqlCommand(sb.ToString(), _ps);
            var reader = cmd.ExecuteReader();
            Entities.Permissions na;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    na = new Entities.Permissions();
                    na.roles = new Entities.Roles();
                    na.elements = new Entities.Elements();

                    na.roles.role_id = int.Parse(reader["role_id"].ToString());
                    na.roles.role_name = reader["role_name"].ToString();
                    na.roles.role_description = reader["role_description"].ToString();

                    na.elements.element_id = string.IsNullOrEmpty(reader["element_id"].ToString()) ? new int?() : int.Parse(reader["element_id"].ToString());
                    na.elements.element_type = reader["element_type"].ToString();
                    result.Add(na);
                }
            }
            reader.Close();
            _ps.Close();

            return result;
        }

        public System.Data.DataTable GetPermissionsAsDatatable()
        {
            StringBuilder sb = new StringBuilder();
            List<Entities.Permissions> result = new List<Entities.Permissions>();
            sb.Append(QueryGetPermissions());
            //_ps.OpenConnectionAndCreateCommand();
            //return _ps.ExecuteDataTable(sb.ToString());
            return null;
        }

        private string QueryGetPermissions() {
            StringBuilder sb = new StringBuilder();            
            sb.Append(" select  ");
            sb.Append("     r.*, e.element_id,e.element_type ");
            sb.Append(" from roles r left join permissions p on r.role_id = p.role_id ");
            sb.Append("             left join elements e on p.element_id = e.element_id ");
            return sb.ToString();
        }
        
        public List<Entities.Elements> GetElementsPermittedByRoleID(int role_id)
        {
            StringBuilder sb = new StringBuilder();
            List<Entities.Elements> result = new List<Entities.Elements>();
            sb.Append(" select   ");
            sb.Append("     e.element_id,e.element_type  ");
            sb.Append(" from roles r join permissions p on r.role_id = p.role_id  ");
            sb.Append("   join elements e on p.element_id = e.element_id  ");
            sb.Append(" where r.role_id = @valroleid ");

            _ps.Open();

            MySqlCommand cmd = new MySqlCommand(sb.ToString(), _ps);
            cmd.Parameters.AddWithValue("@valroleid", role_id);
            var reader = cmd.ExecuteReader();
            Entities.Elements na;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    na = new Entities.Elements();                 
                    na.element_id = string.IsNullOrEmpty(reader["element_id"].ToString()) ? new int?() : int.Parse(reader["element_id"].ToString());
                    na.element_type = reader["element_type"].ToString();
                    result.Add(na);
                }
            }
            reader.Close();
            _ps.Close();
            return result;
        }
        
        public int InsertPermission(int role_id, int element_id)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append("insert into permissions (role_id, element_id) ");
            sb.Append(" values (@valroleid, @valelementid) ");           
            
            _ps.Open();

            MySqlCommand cmd = new MySqlCommand(sb.ToString(), _ps);
            cmd.Parameters.AddWithValue("@valroleid", role_id);
            cmd.Parameters.AddWithValue("@valelementid", element_id);
            returnValue = cmd.ExecuteNonQuery();

            _ps.Close();
            return returnValue;
        }

        public bool ThisRoleHavePermissionFor(int role_id, int element_id)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select count(*) jml ");
            sb.Append(" from  permissions ");
            sb.Append(" where role_id = @valroleid and element_id = @valelementid ");

            _ps.Open();

            MySqlCommand cmd = new MySqlCommand(sb.ToString(), _ps);
            cmd.Parameters.AddWithValue("@valroleid", role_id);
            cmd.Parameters.AddWithValue("@valelementid", element_id);
            string result = cmd.ExecuteScalar().ToString();

            _ps.Close();
            return (result != "0");
        }

        public List<Entities.Elements> AvailableElementsForRoleID(int role_id)
        {
            StringBuilder sb = new StringBuilder();
            List<Entities.Elements> result = new List<Entities.Elements>();
            sb.Append(" select element_id, element_type  ");
            sb.Append(" from elements ");
            sb.Append(" where element_id not in ");
            sb.Append(" ( ");
            sb.Append(" select   ");
            sb.Append(" 	e.element_id ");
            sb.Append(" from roles r join permissions p on r.role_id = p.role_id  ");
            sb.Append("   join elements e on p.element_id = e.element_id  ");
            sb.Append(" where r.role_id = @valroleid ");
            sb.Append(" ) ");
            _ps.Open();

            MySqlCommand cmd = new MySqlCommand(sb.ToString(), _ps);
            cmd.Parameters.AddWithValue("@valroleid", role_id);
            var reader = cmd.ExecuteReader();
            Entities.Elements na;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    na = new Entities.Elements();
                    na.element_id = string.IsNullOrEmpty(reader["element_id"].ToString()) ? new int?() : int.Parse(reader["element_id"].ToString());
                    na.element_type = reader["element_type"].ToString();
                    result.Add(na);
                }
            }
            reader.Close();
            _ps.Close();
            return result;
        }

        public int DeletePermission(int role_id, int element_id)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append("delete from  permissions ");
            sb.Append(" where role_id = @valroleid and element_id = @valelementid ");

            _ps.Open();

            MySqlCommand cmd = new MySqlCommand(sb.ToString(), _ps);
            cmd.Parameters.AddWithValue("@valroleid", role_id);
            cmd.Parameters.AddWithValue("@valelementid", element_id);
            returnValue = cmd.ExecuteNonQuery();

            _ps.Close();
            return returnValue;
        }

        public List<Entities.Users> UsersHaveThisRole(int role_id)
        {
            StringBuilder sb = new StringBuilder();
            List<Entities.Users> result = new List<Entities.Users>();
            sb.Append(" select uir.user_id, u.user_name ");
            sb.Append(" from roles r join usersinroles uir on r.role_id = uir.role_id ");
            sb.Append("                         join users u on uir.user_id = u.user_id ");
            sb.Append(" where r.role_id = @valroleid ");
            _ps.Open();

            MySqlCommand cmd = new MySqlCommand(sb.ToString(), _ps);
            cmd.Parameters.AddWithValue("@valroleid", role_id);
            var reader = cmd.ExecuteReader();
            Entities.Users na;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    na = new Entities.Users();
                    na.user_id =  int.Parse(reader["user_id"].ToString());
                    na.user_name = reader["user_name"].ToString();
                    result.Add(na);
                }
            }
            reader.Close();
            _ps.Close();
            return result;
        }
    }
}
