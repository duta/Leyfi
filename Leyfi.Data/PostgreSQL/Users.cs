﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using DBProvider;
using Npgsql;
using Leyfi.Data.Interfaces;

namespace Leyfi.Data.PostgreSQL
{
    internal class Users : IUsers
    {
        DBProvider.PostgreSQL _ps;

        public Users(string connStr)
        {
            _ps = new DBProvider.PostgreSQL(connStr);
        }

        public List<Entities.Users> GetAll()
        {
            StringBuilder sb = new StringBuilder();
            List<Entities.Users> result = new List<Entities.Users>();
            sb.Append(" select ");
            sb.Append(" 	user_id,user_name, to_char(last_login, 'dd-mm-yyyy HH24:mi:ss') last_login, to_char(last_logout, 'dd-mm-yyyy HH24:mi:ss') last_logout ");
            sb.Append(" from moresecure.users  "); 

            _ps.OpenConnectionAndCreateCommand();
            NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString());
            Entities.Users na;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    na = new Entities.Users();
                    na.user_id = int.Parse(reader["user_id"].ToString());
                    na.user_name = reader["user_name"].ToString();
                    na.last_login = string.IsNullOrEmpty(reader["last_login"].ToString()) ? new DateTime?() : DateTime.ParseExact(reader["last_login"].ToString(), "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    na.last_logout = string.IsNullOrEmpty(reader["last_logout"].ToString()) ? new DateTime?() : DateTime.ParseExact(reader["last_logout"].ToString(), "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);                  
                    result.Add(na);
                }
            }
            reader.Close();
            _ps.CloseConnection();

            return result;
        }


        public List<Entities.UsersInRoles> GetUserInRoles(string user_name)
        {
            StringBuilder sb = new StringBuilder();
            List<Entities.UsersInRoles> result = new List<Entities.UsersInRoles>();
            //sb.Append(" select ");
            //sb.Append(" 	user_id,user_name, to_char(last_activity, 'dd-mm-yyyy HH24:mi:ss') last_activity ");
            //sb.Append(" from moresecure.users  ");

            _ps.OpenConnectionAndCreateCommand();
            NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString());
            Entities.UsersInRoles na;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    //na = new Entities.Users();
                    //na.user_id = int.Parse(reader["user_id"].ToString());
                    //na.user_name = reader["user_name"].ToString();
                    //na.last_activity = string.IsNullOrEmpty(reader["last_activity"].ToString()) ? new DateTime?() : DateTime.ParseExact(reader["last_activity"].ToString(), "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    //result.Add(na);
                }
            }
            reader.Close();
            _ps.CloseConnection();

            return result;
        }


        public int Delete(int user_id)
        {
            int returnValue = 0;
            StringBuilder sb;
            NpgsqlTransaction trans = null;

            try
            {
                _ps.OpenConnectionAndCreateCommand();
                using (trans = _ps.Comm.Connection.BeginTransaction())
                {
                    using (_ps.Comm)
                    {
                        _ps.Comm.Transaction = trans;

                        sb = new StringBuilder();
                        sb.Append("delete from  moresecure.usersinroles ");
                        sb.Append(" where user_id = :valuserid  ");

                        NpgsqlParameter paramUserId;
                        paramUserId = new NpgsqlParameter("valuserid", NpgsqlTypes.NpgsqlDbType.Integer);

                        NpgsqlParameter[] Params = new NpgsqlParameter[] { paramUserId };
                        paramUserId.Value = user_id;
                        _ps.Comm.Parameters.AddRange(Params);
                        _ps.Comm.CommandText = sb.ToString();
                        returnValue += _ps.Comm.ExecuteNonQuery();
                        
                        sb = new StringBuilder();
                        sb.Append(" delete from  moresecure.users ");
                        sb.Append(" where user_id = :valuserid ");
                        paramUserId.Value = user_id;
                        _ps.Comm.Parameters.AddRange(Params);
                        _ps.Comm.CommandText = sb.ToString();
                        returnValue += _ps.Comm.ExecuteNonQuery();

                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                if (trans != null) trans.Rollback();
            }
            finally
            {
                _ps.CloseConnection();
            }

            return returnValue;
        }

        public int InsertUsersInRoles(int role_id, int user_id)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append("insert into moresecure.usersinroles (role_id, user_id) ");
            sb.Append(" values (:valroleid, :valuserid) ");

            NpgsqlParameter paramRoleId, paramUserID;
            paramRoleId = new NpgsqlParameter("valroleid", NpgsqlTypes.NpgsqlDbType.Integer);
            paramUserID = new NpgsqlParameter("valuserid", NpgsqlTypes.NpgsqlDbType.Integer);

            NpgsqlParameter[] Params;
            Params = new NpgsqlParameter[] { paramRoleId, paramUserID };
            paramRoleId.Value = role_id;
            paramUserID.Value = user_id;

            _ps.OpenConnectionAndCreateCommand();
            returnValue = _ps.ExecuteNonQuery(sb.ToString(), Params);
            return returnValue;
        }

        public int DeleteUsersInRoles(int role_id, int user_id)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append("delete from  moresecure.usersinroles ");
            sb.Append(" where role_id = :valroleid and user_id = :valuserid ");

            NpgsqlParameter paramRoleId, paramUserID;
            paramRoleId = new NpgsqlParameter("valroleid", NpgsqlTypes.NpgsqlDbType.Integer);
            paramUserID = new NpgsqlParameter("valuserid", NpgsqlTypes.NpgsqlDbType.Integer);

            NpgsqlParameter[] Params;
            Params = new NpgsqlParameter[] { paramRoleId, paramUserID };
            paramRoleId.Value = role_id;
            paramUserID.Value = user_id;

            _ps.OpenConnectionAndCreateCommand();
            returnValue = _ps.ExecuteNonQuery(sb.ToString(), Params);
            return returnValue;
        }

        public int Upsert(int? user_id, string user_name, byte[] password)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            if (user_id.HasValue)
            {
                sb.Append("update moresecure.users set user_name= :valusername, password = :valpassword ");
                sb.Append("    where user_id = :valuserid");
            }
            else
            {
                sb.Append("insert into moresecure.users (user_name, password) ");
                sb.Append(" values (:valusername, :valpassword ) ");
            }

            NpgsqlParameter paramId, paramName, paramPassword;
            paramId = new NpgsqlParameter("valuserid", NpgsqlTypes.NpgsqlDbType.Integer);
            paramName = new NpgsqlParameter("valusername", NpgsqlTypes.NpgsqlDbType.Text);
            paramPassword = new NpgsqlParameter("valpassword", NpgsqlTypes.NpgsqlDbType.Bytea);

            NpgsqlParameter[] Params;
            if (user_id.HasValue)
            {
                Params = new NpgsqlParameter[] { paramName, paramPassword,paramId};
                paramId.Value = user_id.Value;
            }
            else
            {
                Params = new NpgsqlParameter[] { paramName, paramPassword};
            }
            paramName.Value = user_name;
            paramPassword.Value = password;

            _ps.OpenConnectionAndCreateCommand();
            returnValue = _ps.ExecuteNonQuery(sb.ToString(), Params);
            return returnValue;
        }

        public List<Entities.Roles> GetRolesByUserID(int user_id)
        {
            StringBuilder sb = new StringBuilder();            
            sb.Append(" select ");
            sb.Append("     r.* ");
            sb.Append(" from moresecure.users u  join moresecure.usersinroles uir on u.user_id = uir.user_id ");
            sb.Append("      join moresecure.roles r on uir.role_id = r.role_id ");
            sb.Append(" where u.user_id = :valuserid  ");
            NpgsqlParameter paramId;
            paramId = new NpgsqlParameter("valuserid", NpgsqlTypes.NpgsqlDbType.Integer);
            NpgsqlParameter[] Params= new NpgsqlParameter[] { paramId };
            paramId.Value = user_id; 

            _ps.OpenConnectionAndCreateCommand();
            NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString(),Params);
            Entities.Roles na;
            List<Entities.Roles> result = new List<Entities.Roles>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    na = new Entities.Roles();
                    na.role_id = int.Parse(reader["role_id"].ToString());
                    na.role_name = reader["role_name"].ToString();
                    na.role_description = reader["role_description"].ToString();
                    result.Add(na);
                }
            }
            reader.Close();
            _ps.CloseConnection();

            return result;

        }


        public List<Entities.Roles> AvailableRolesForUserID(int user_id)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select  ");
            sb.Append("   r.*	 ");
            sb.Append(" from moresecure.roles r ");
            sb.Append(" where  ");
            sb.Append("   r.role_id not in ( ");
            sb.Append(" 	select ");
            sb.Append(" 		r.role_id ");
            sb.Append(" 	from moresecure.users u join moresecure.usersinroles uir on u.user_id = uir.user_id ");
            sb.Append("          join moresecure.roles r on uir.role_id = r.role_id ");
            sb.Append("     where u.user_id = :valuserid ");
            sb.Append(" ) ");
            NpgsqlParameter paramId;
            paramId = new NpgsqlParameter("valuserid", NpgsqlTypes.NpgsqlDbType.Integer);
            NpgsqlParameter[] Params = new NpgsqlParameter[] { paramId };
            paramId.Value = user_id;

            _ps.OpenConnectionAndCreateCommand();
            NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString(), Params);
            Entities.Roles na;
            List<Entities.Roles> result = new List<Entities.Roles>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    na = new Entities.Roles();
                    na.role_id = int.Parse(reader["role_id"].ToString());
                    na.role_name = reader["role_name"].ToString();
                    na.role_description = reader["role_description"].ToString();
                    result.Add(na);
                }
            }
            reader.Close();
            _ps.CloseConnection();

            return result;
        }


        public bool LastLoginExpired(int user_id, int max_hour_expired)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select  to_char(last_login, 'dd-mm-yyyy HH24:mi:ss')  last_login ");
            sb.Append(" from  moresecure.users ");
            sb.Append(" where user_id = :valuserid  ");
            NpgsqlParameter paramUserId;
            paramUserId = new NpgsqlParameter("valuserid", NpgsqlTypes.NpgsqlDbType.Integer);
           
            NpgsqlParameter[] Params;
            Params = new NpgsqlParameter[] { paramUserId };
            paramUserId.Value = user_id;

            _ps.OpenConnectionAndCreateCommand();
            string result = _ps.ExecuteScalar(sb.ToString(), Params);
            if (string.IsNullOrEmpty(result)) return true;
            DateTime resultDate = DateTime.ParseExact(result, "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            TimeSpan span = DateTime.Now.Subtract(resultDate);
            return span.TotalHours > max_hour_expired;
        }


        public bool LastLogoutNull(int user_id)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select  to_char(last_logout, 'dd-mm-yyyy HH24:mi:ss')  last_logout ");
            sb.Append(" from  moresecure.users ");
            sb.Append(" where user_id = :valuserid  ");
            NpgsqlParameter paramUserId;
            paramUserId = new NpgsqlParameter("valuserid", NpgsqlTypes.NpgsqlDbType.Integer);

            NpgsqlParameter[] Params;
            Params = new NpgsqlParameter[] { paramUserId };
            paramUserId.Value = user_id;

            _ps.OpenConnectionAndCreateCommand();
            string result = _ps.ExecuteScalar(sb.ToString(), Params);
            return string.IsNullOrEmpty(result);
        }

        public int SetUserLogOut(int user_id)
        {
            return SetUserLog(user_id, false);
        }

        public int SetUserLogIn(int user_id)
        {
            return SetUserLog(user_id, true);
        }

        private int SetUserLog(int user_id, bool isLogin) {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();

            sb.Append("update moresecure.users set ");
            if (isLogin)
            {
                sb.Append(" last_login = now(), last_logout = NULL ");
            }
            else {
                sb.Append(" last_logout= now() ");                
            }
            sb.Append("    where user_id = :valuserid");

            NpgsqlParameter paramId;
            paramId = new NpgsqlParameter("valuserid", NpgsqlTypes.NpgsqlDbType.Integer);

            NpgsqlParameter[] Params;           
            Params = new NpgsqlParameter[] {  paramId };
            paramId.Value = user_id;          

            _ps.OpenConnectionAndCreateCommand();
            returnValue = _ps.ExecuteNonQuery(sb.ToString(), Params);
            return returnValue;
        }

        public int? GetUserIDByUserName(string user_name) {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select  user_id ");
            sb.Append(" from  moresecure.users ");
            sb.Append(" where upper(user_name) = :valusername  ");
            NpgsqlParameter paramUser;
            paramUser = new NpgsqlParameter("valusername", NpgsqlTypes.NpgsqlDbType.Text);

            NpgsqlParameter[] Params;
            Params = new NpgsqlParameter[] { paramUser };
            paramUser.Value = user_name.ToUpper();

            _ps.OpenConnectionAndCreateCommand();
            string result = _ps.ExecuteScalar(sb.ToString(), Params);
            if (string.IsNullOrEmpty(result)) return new Nullable<int>();
            return int.Parse(result);
        }

        public byte[] GetPasswordByUserId(int user_id) {
            byte[] result = {};
            StringBuilder sb = new StringBuilder();
            sb.Append(" select ");
            sb.Append(" 	password ");
            sb.Append(" from moresecure.users  ");
            sb.Append(" where user_id = :valuserid  ");
            NpgsqlParameter paramId;
            paramId = new NpgsqlParameter("valuserid", NpgsqlTypes.NpgsqlDbType.Integer);

            NpgsqlParameter[] Params;
            Params = new NpgsqlParameter[] { paramId };
            paramId.Value = user_id;       
            _ps.OpenConnectionAndCreateCommand();
            NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString(), Params);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    if (reader[0] != DBNull.Value)
                    {
                        result = (byte[])reader[0];
                    }
                }
            }
            reader.Close();
            _ps.CloseConnection();
            return result;
        }


        public Entities.Users GetUserByUserID(int user_id)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select ");
            sb.Append(" 	user_id,user_name, to_char(last_login, 'dd-mm-yyyy HH24:mi:ss') last_login, to_char(last_logout, 'dd-mm-yyyy HH24:mi:ss') last_logout, password ");
            sb.Append(" from moresecure.users  ");
            sb.Append(" where user_id = :valuserid ");
            NpgsqlParameter paramId;
            paramId = new NpgsqlParameter("valuserid", NpgsqlTypes.NpgsqlDbType.Integer);
            NpgsqlParameter[] Params;
            Params = new NpgsqlParameter[] { paramId };
            paramId.Value = user_id;

            Entities.Users na = new Entities.Users();
            _ps.OpenConnectionAndCreateCommand();
            NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString(), Params);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    na = new Entities.Users();
                    na.user_id = int.Parse(reader["user_id"].ToString());
                    na.user_name = reader["user_name"].ToString();
                    na.last_login = string.IsNullOrEmpty(reader["last_login"].ToString()) ? new DateTime?() : DateTime.ParseExact(reader["last_login"].ToString(), "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    na.last_logout = string.IsNullOrEmpty(reader["last_logout"].ToString()) ? new DateTime?() : DateTime.ParseExact(reader["last_logout"].ToString(), "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    na.password = (byte[])reader["password"];
                }
            }
            reader.Close();
            _ps.CloseConnection();

            return na;
        }
    }
}
