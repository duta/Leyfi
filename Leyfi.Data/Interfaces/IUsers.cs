﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Leyfi.Data.Entities;

namespace Leyfi.Data.Interfaces
{
    public interface IUsers
    {
        List<Users> GetAll();
        List<UsersInRoles> GetUserInRoles(string user_name);
        List<Roles> GetRolesByUserID(int user_id);
        List<Entities.Roles> AvailableRolesForUserID(int user_id);
        int Delete(int user_id);
        int SetUserLogOut(int user_id);
        int SetUserLogIn(int user_id);
        int InsertUsersInRoles(int role_id, int user_id);
        int DeleteUsersInRoles(int role_id, int user_id);
        int Upsert(int? user_id, string user_name, byte[] password);
        int? GetUserIDByUserName(string user_name);
        bool LastLoginExpired(int user_id, int max_hour_expired);
        bool LastLogoutNull(int user_id);
        byte[] GetPasswordByUserId(int user_id);
        Users GetUserByUserID(int user_id);
    }
}
