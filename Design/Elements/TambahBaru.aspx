﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default/Default.Master" AutoEventWireup="true" CodeFile="TambahBaru.aspx.cs" Inherits="Elements_TambahBaru" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <div class="hero-unit">
        <div class="form-horizontal">
            <legend><asp:Label ID="lblLegend" runat="server" Text="Tambah Element"></asp:Label></legend>
            <div class="control-group">                
                <label class="control-label" >Nama Element</label>
                <div class="controls">
                 <asp:TextBox ID="txtNama" runat="server" ClientIDMode="Static"></asp:TextBox>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                <label class="checkbox"></label>
                    <asp:Button ID="btnSimpan" runat="server" Text="Simpan" CssClass="btn" OnClick="btnSimpan_Click" />
                </div>
            </div>
        </div>
    </div>   
    <script src="../Scripts/Elements.js"></script> 
</asp:Content>

