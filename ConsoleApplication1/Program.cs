﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Linq;
using System.Text;
using Microsoft.VisualBasic;

namespace ConsoleApplication1
{
    [DataContract]   
    public class DigDown {
        [DataMember]
        public string aivi;
        [DataMember]        
        public string sesi;
        [DataMember]        
        public string content;
    } 


    class Program
    {
        static void Main(string[] args)
        {
            using (Bob bob = new Bob())
            {
                using (RSACryptoServiceProvider rsaKey = new RSACryptoServiceProvider())
                {
                    // Get Bob's public key
                    rsaKey.ImportCspBlob(bob.key);
                    byte[] encryptedSessionKey = null;
                    byte[] encryptedMessage = null;
                    byte[] iv = null;
                    Send(rsaKey, "Secret message", out iv, out encryptedSessionKey, out encryptedMessage);
                    bob.Receive(iv, encryptedSessionKey, encryptedMessage);
                }
            }
        }

        private static void Send(RSA key, string secretMessage, out byte[] iv, out byte[] encryptedSessionKey, out byte[] encryptedMessage)
        {
            using (Aes aes = new AesCryptoServiceProvider())
            {
                iv = aes.IV;

                // Encrypt the session key
                RSAOAEPKeyExchangeFormatter keyFormatter = new RSAOAEPKeyExchangeFormatter(key);
                encryptedSessionKey = keyFormatter.CreateKeyExchange(aes.Key, typeof(Aes));

                // Encrypt the message
                using (MemoryStream ciphertext = new MemoryStream())
                using (CryptoStream cs = new CryptoStream(ciphertext, aes.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    byte[] plaintextMessage = Encoding.UTF8.GetBytes(secretMessage);
                    cs.Write(plaintextMessage, 0, plaintextMessage.Length);
                    cs.Close();

                    encryptedMessage = ciphertext.ToArray();
                }
                //return;
                DigDown dig = new DigDown();
                dig.aivi = Convert.ToBase64String(iv);
                dig.sesi = Convert.ToBase64String(encryptedSessionKey);
                dig.content = Convert.ToBase64String(encryptedMessage);
                MemoryStream stream1 = new MemoryStream();
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(DigDown));
                ser.WriteObject(stream1, dig);
                string jsonString = Encoding.UTF8.GetString(stream1.ToArray());
                stream1.Close();

                //Encryption
                string passToSave = Encrypt(jsonString, "&%#@?,:*");
                //Finish Encrypt

                //Decrypt 
                string savedPassword = Decrypt(passToSave, "&%#@?,:*");
                stream1 = new MemoryStream(Encoding.UTF8.GetBytes(savedPassword));
                dig =  (DigDown)ser.ReadObject(stream1);

                iv = Convert.FromBase64String(dig.aivi);
                encryptedSessionKey = Convert.FromBase64String(dig.sesi);
                encryptedMessage = Convert.FromBase64String(dig.content);
                stream1.Close();
            }
        }

        public static string Decrypt(string pText, string pDecrKey)
        {
            string str = "";
            byte[] rgbIV = new byte[] { 0x12, 0x34, 0x56, 120, 0x90, 0xab, 0xcd, 0xef };
            byte[] buffer = new byte[pText.Length + 1];
            try
            {
                byte[] bytes = Encoding.UTF8.GetBytes(Strings.Left(pDecrKey, 8));
                DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
                buffer = Convert.FromBase64String(pText);
                MemoryStream stream2 = new MemoryStream();
                CryptoStream stream = new CryptoStream(stream2, provider.CreateDecryptor(bytes, rgbIV), CryptoStreamMode.Write);
                stream.Write(buffer, 0, buffer.Length);
                stream.FlushFinalBlock();
                str = Encoding.UTF8.GetString(stream2.ToArray());
            }
            catch (Exception exception1)
            {
                str = "Decrypt error" + exception1.StackTrace;
            }
            return str;
        }

        public static string Encrypt(string pText, string pEncrKey)
        {
            string message = "";
            byte[] rgbIV = new byte[] { 0x12, 0x34, 0x56, 120, 0x90, 0xab, 0xcd, 0xef };
            try
            {
                byte[] bytes = Encoding.UTF8.GetBytes( Strings.Left(pEncrKey, 8));
                byte[] buffer = Encoding.UTF8.GetBytes(pText);
                DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
                MemoryStream stream2 = new MemoryStream();
                CryptoStream stream = new CryptoStream(stream2, provider.CreateEncryptor(bytes, rgbIV), CryptoStreamMode.Write);
                stream.Write(buffer, 0, buffer.Length);
                stream.FlushFinalBlock();
                message = Convert.ToBase64String(stream2.ToArray());
            }
            catch (Exception exception1)
            {
                message = "Encrypt error" + exception1.StackTrace;
            }
            return message;
        }
    }

           
    public class Bob : IDisposable
    {
        public byte[] key;
        private RSACryptoServiceProvider rsaKey = new RSACryptoServiceProvider();
        public Bob()
        {
            key = rsaKey.ExportCspBlob(false);
        }
        public void Receive(byte[] iv, byte[] encryptedSessionKey, byte[] encryptedMessage)
        {

            using (Aes aes = new AesCryptoServiceProvider())
            {
                aes.IV = iv;

                // Decrypt the session key
                RSAOAEPKeyExchangeDeformatter keyDeformatter = new RSAOAEPKeyExchangeDeformatter(rsaKey);
                aes.Key = keyDeformatter.DecryptKeyExchange(encryptedSessionKey);

                // Decrypt the message
                using (MemoryStream plaintext = new MemoryStream())
                using (CryptoStream cs = new CryptoStream(plaintext, aes.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(encryptedMessage, 0, encryptedMessage.Length);
                    cs.Close();

                    string message = Encoding.UTF8.GetString(plaintext.ToArray());
                    Console.WriteLine(message);
                }
            }
        }
        public void Dispose()
        {
            rsaKey.Dispose();
        }
    }
}
