﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default/Default.Master" AutoEventWireup="true" CodeFile="TambahBaruRole.aspx.cs" Inherits="Users_TambahBaruRole" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <div class="hero-unit">
        <div class="form-horizontal">
            <legend>
                <asp:Label ID="lblLegend" runat="server" Text="Tambah Role"></asp:Label></legend>
            <div class="control-group">
                <label class="control-label">
                    Nama Role</label>
                <div class="controls">
                    <telerik:RadComboBox ID="cmbRoles" runat="server" DataTextField="role_name"
                        DataValueField="role_id" EnableAutomaticLoadOnDemand="true" ShowMoreResultsBox="true"
                        EnableVirtualScrolling="true" ItemsPerRequest="10" Filter="Contains">
                    </telerik:RadComboBox>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <label class="checkbox"></label>
                    <asp:Button ID="btnSimpan" runat="server" Text="Simpan" CssClass="btn" OnClick="btnSimpan_Click" />
                </div>
            </div>
        </div>
    </div>
    <script src="../Scripts/Users.js"></script>
</asp:Content>

