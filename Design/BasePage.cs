﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Leyfi.Data.Interfaces;

namespace Leyfi
{
    public class BasePage : System.Web.UI.Page
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }

        public static IFactory CreateFactory()
        {
            IFactory factory;

            if (HttpContext.Current.Session["factoryLeyfi"] == null)
                HttpContext.Current.Session["factoryLeyfi"] = (IFactory)Global.WindsorContainer["FactoryLeyfi"];
            factory = (IFactory)HttpContext.Current.Session["factoryLeyfi"];

            return factory;
        }
    }
}