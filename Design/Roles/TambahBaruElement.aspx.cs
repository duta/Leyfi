﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Leyfi;
using Leyfi.Data.Entities;

public partial class Roles_TambahBaruElement : BasePage
{
    private Roles _roles;

    protected void Page_Load(object sender, EventArgs e)
    {
        _roles = (Roles)Session["EditOpRoles"];
        FillComboElement();   
    }  

    protected void btnSimpan_Click(object sender, EventArgs e)
    {
        if (cmbElements.SelectedValue == "") return;
        CreateFactory().GetRoles().InsertPermission(_roles.role_id.Value, int.Parse(cmbElements.SelectedValue));
        Response.Redirect("TambahBaru.aspx");
    }

    private void FillComboElement() {
        cmbElements.DataSource = CreateFactory().GetRoles().AvailableElementsForRoleID(_roles.role_id.Value);
        
    }
}