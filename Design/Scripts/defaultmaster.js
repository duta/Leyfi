﻿/// <reference path="jquery-1.7.2.min.js" />
$(document).ready(function () {
    $("#copyrights").html("<p id='#copyrights'>&copy; " + (new Date).getFullYear() + ". All Rights Reserved.</p>");
    //info pop out menu
    var optionsPopOverMenuUsersDaftar = { trigger: 'hover', content: '<p>Daftar pengguna aplikasi</p>' };
    var optionsPopOverMenuRolesDaftar = { trigger: 'hover', content: '<p>Daftar role/peran seperti :</p><ul><li>Administrator</li><li>Members</li><li>dan lainnya</li></ul>' };
    var optionsPopOverMenuElementsDaftar = { trigger: 'hover', content: '<p>Daftar element bisa berupa :</p><ul><li>Aplikasi</li><li>Fitur Aplikasi</li><li>Menu</li></ul>' };
    $('#menuUsersDaftar').popover(optionsPopOverMenuUsersDaftar);
    $('#menuRolesDaftar').popover(optionsPopOverMenuRolesDaftar);
    $('#menuElementsDaftar').popover(optionsPopOverMenuElementsDaftar);    
});



