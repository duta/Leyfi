﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Leyfi;
using Leyfi.Data.Entities;

public partial class Roles_TambahBaru : BasePage
{
    private Roles _roles;
    private bool edit_mode { get { return _roles != null; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        _roles = (Roles)Session["EditOpRoles"];
        if (edit_mode)
        {
            lblLegend.Text = "Edit Role";
            if (!IsPostBack) {
                txtNama.Text = _roles.role_name;
                txtDesc.Text = _roles.role_description;
                lblPermission.Text += txtNama.Text;
            }            
        }
        else
        {
            lblLegend.Text = "Tambah Roles";
        }
        SetVisibleDaftarElements(edit_mode);  
    }

    protected void btnSimpan_Click(object sender, EventArgs e)
    {
        CreateFactory().GetRoles().Upsert(!edit_mode ? new int?() : _roles.role_id,  txtNama.Text.Trim(), txtDesc.Text.Trim());
        if (edit_mode) {
            _roles.role_name = txtNama.Text.Trim();
            _roles.role_description = txtDesc.Text.Trim();
            lblPermission.Text += txtNama.Text.Trim();
            Session["EditOpRoles"] = _roles;
        }
        else {
            Response.Redirect("Daftar.aspx");
        }        
    }

    protected void GridDaftar_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            Elements selected_element = ((Elements)e.Item.DataItem);
            CreateFactory().GetRoles().DeletePermission(_roles.role_id.Value, selected_element.element_id.Value);
            GridDaftar_NeedDataSource(null, null);
        }
        else if (e.CommandName == "InitInsert")
        {
            Response.Redirect("TambahBaruElement.aspx");
        }
        else if (e.CommandName == "RebindGrid")
        {
            GridDaftar.Rebind();
        }
    }

    protected void GridDaftar_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        List<Leyfi.Data.Entities.Elements> elements = new List<Elements>();
        if (edit_mode) {
            elements = CreateFactory().GetRoles().GetElementsPermittedByRoleID(_roles.role_id.Value);
        }
        GridDaftar.DataSource = elements;
    }

    private void SetVisibleDaftarElements(bool val) {
        lblPermission.Visible = val;
        GridDaftar.Visible = val;
    }
}