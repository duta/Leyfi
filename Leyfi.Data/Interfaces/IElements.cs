﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Leyfi.Data.Entities;

namespace Leyfi.Data.Interfaces
{
    public interface IElements
    {
        List<Elements> GetAll();
        int Upsert(int? element_id, string element_type);
        int Delete(int element_id);
    }
}
