﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using Leyfi.Data.Interfaces;

namespace Leyfi.Data.MySQL
{
    internal class Elements : IElements
    {
        MySqlConnection _ps;

        public Elements(string connStr)
        {
            _ps = new MySqlConnection(connStr);
        }
    
        public List<Entities.Elements>  GetAll()
        {
            StringBuilder sb = new StringBuilder();
            List<Entities.Elements> result = new List<Entities.Elements>();
            sb.Append(" select ");
            sb.Append(" 	element_id,element_type  ");
            sb.Append(" from elements  ");

            _ps.Open();

            MySqlCommand cmd = new MySqlCommand(sb.ToString(), _ps);
            var reader = cmd.ExecuteReader();
            Entities.Elements na;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    na = new Entities.Elements();
                    na.element_id = int.Parse(reader["element_id"].ToString());
                    na.element_type = reader["element_type"].ToString();
                    result.Add(na);
                }
            }
            reader.Close();
            _ps.Close();

            return result;
        }
        
        public int Upsert(int? element_id, string element_type)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            if (element_id.HasValue)
            {
                sb.Append("update elements set element_type=@valelementtype ");
                sb.Append("    where element_id = @valelementid");
            }
            else
            {
                sb.Append("insert into elements (element_type) ");
                sb.Append(" values (@valelementtype) ");
            }

            _ps.Open();

            MySqlCommand cmd = new MySqlCommand(sb.ToString(), _ps);
            if (element_id.HasValue)
            {
                cmd.Parameters.AddWithValue("@valelementid", element_id.Value);
            }
            cmd.Parameters.AddWithValue("@valelementtype", element_type);
            returnValue = cmd.ExecuteNonQuery();

            _ps.Close();
            return returnValue;
        }


        public int Delete(int element_id)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append(" delete from  elements ");
            sb.Append(" where element_id = @valelementid ");
            
            _ps.Open();

            MySqlCommand cmd = new MySqlCommand(sb.ToString(), _ps);
            cmd.Parameters.AddWithValue("@valelementid", element_id);
            returnValue = cmd.ExecuteNonQuery();

            _ps.Close();
            return returnValue;
        }
    }
}
