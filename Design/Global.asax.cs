﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using log4net.Config;
using Castle.Windsor;
using Castle.Windsor.Configuration.Interpreters;

namespace Leyfi
{
    public class Global : System.Web.HttpApplication, IContainerAccessor
    {
        public IWindsorContainer Container
        {
            get { return windsorContainer; }
        }

        /// <summary>
        /// Provides a globally available access to the <see cref="IWindsorContainer" /> instance.
        /// </summary>
        public static IWindsorContainer WindsorContainer
        {
            get { return windsorContainer; }
        }

        /// <summary>
        /// Gets instantiated on <see cref="Application_Start" />.
        /// </summary>
        private static IWindsorContainer windsorContainer;

        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            // Initialize log4net
            XmlConfigurator.Configure();
            // Create the Windsor Container for IoC.
            // Supplying "XmlInterpreter" as the parameter tells Windsor 
            // to look at web.config for any necessary configuration.
            windsorContainer = new WindsorContainer(new XmlInterpreter());
            Application["OnlineUsers"] = 0;
        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown

        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

        }

        void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started
            Application.Lock();
            Application["OnlineUsers"] = (int)Application["OnlineUsers"] + 1;
            Application.UnLock();
        }

        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.
            Application.Lock();
            Application["OnlineUsers"] = (int)Application["OnlineUsers"] - 1;
            Application.UnLock();
        }


       
    }
}
