﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Leyfi;
using Leyfi.Data.Entities;
using Telerik.Web.UI;

public partial class Roles_Daftar : BasePage
{
    private int _selectedRoleID;
    private List<Users> _usersHaveRole = new List<Users>();

    protected void Page_Load(object sender, EventArgs e)
    {
        Session["EditOpRoles"] = null;
        PanelUsers.Visible = _usersHaveRole.Count > 0;
        if (!Page.IsPostBack) {
            GridDaftar.Rebind();
        }   
    }

    protected void GridDaftar_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            _selectedRoleID  = ((Roles)e.Item.DataItem).role_id.Value;
            //TO-DO: Checking apakah role sedang digunakan user   
            _usersHaveRole =  CreateFactory().GetRoles().UsersHaveThisRole(_selectedRoleID);
            PanelUsers.Visible = _usersHaveRole.Count > 0;
            if (_usersHaveRole.Count > 0)
            {
                lblWarning.Text = "Role " + ((Roles)e.Item.DataItem).role_name + " masih dimiliki user ";                 
                foreach (var item in _usersHaveRole)
                {
                    lblWarning.Text += item.user_name + ",";
                }
                lblWarning.Text  = lblWarning.Text.Remove(lblWarning.Text.Length - 1, 1);
                HiddenFieldRoleID.Value = _selectedRoleID.ToString();
            }
            else {              
                DeleteAll();
            }            
        }
        else if (e.CommandName == "Edit")
        {
            Session["EditOpRoles"] = ((Roles)e.Item.DataItem);
            Response.Redirect("TambahBaru.aspx");
        }
        else if (e.CommandName == "InitInsert")
        {
            Response.Redirect("TambahBaru.aspx");
        }
        else if (e.CommandName == "RebindGrid")
        {
            GridDaftar.Rebind();
        }
    }

    protected void GridDaftar_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        List< Leyfi.Data.Entities.Roles> roles = CreateFactory().GetRoles().GetAll();
        GridDaftar.DataSource = roles;
    }

    private void DeleteAll() {
        CreateFactory().GetRoles().Delete(_selectedRoleID);
        GridDaftar_NeedDataSource(null, null);
    }

    protected void cmdDeleteAnyway_Click(object sender, EventArgs e)
    {
        _selectedRoleID = int.Parse(HiddenFieldRoleID.Value);
        DeleteAll();
        HiddenFieldRoleID.Value = "";
    }
}