﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default/Default.Master" AutoEventWireup="true" CodeFile="Daftar.aspx.cs" Inherits="Elements_Daftar" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <div class="hero-unit">
        <div class="form-horizontal">
            <legend>
                <asp:Label ID="lblLegend" runat="server" Text="Daftar Element"></asp:Label>
             </legend>
            <telerik:RadGrid ID="GridDaftar" runat="server" GridLines="None" AllowSorting="True"
                OnNeedDataSource="GridDaftar_NeedDataSource" OnItemCommand="GridDaftar_ItemCommand"
                AllowPaging="true" ShowStatusBar="true" Skin="Black">
                <ClientSettings AllowGroupExpandCollapse="true" EnablePostBackOnRowClick="false" >
                </ClientSettings>
                <MasterTableView AutoGenerateColumns="false" Width="100%" CommandItemDisplay="Top" >
                    <Columns>
                        <telerik:GridBoundColumn DataField="element_id" HeaderText="" Visible="false" ReadOnly="true" >
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="element_type" HeaderText="Tipe Element">
                        </telerik:GridBoundColumn>
                        <telerik:GridButtonColumn ButtonCssClass="btn btn-link" Text="Edit" CommandName="Edit" >
                        </telerik:GridButtonColumn>
                        <telerik:GridButtonColumn ButtonCssClass="btn btn-danger" Text="Delete" CommandName="Delete"><ItemStyle ForeColor="White" />
                        </telerik:GridButtonColumn>
                    </Columns>
                    <CommandItemSettings AddNewRecordText="Add Elements" RefreshText="Refresh" ShowRefreshButton="true"
                        ShowAddNewRecordButton="true" />
                </MasterTableView>
                <PagerStyle Mode="NextPrevAndNumeric" />
            </telerik:RadGrid>
        </div>       
    </div>
    <script src="../Scripts/Elements.js"></script>
</asp:Content>

