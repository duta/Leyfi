﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default/Default.Master" AutoEventWireup="true" CodeFile="Daftar.aspx.cs" Inherits="Users_Daftar" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <div class="hero-unit">
        <div class="form-horizontal">
            <legend>
                <asp:Label ID="lblLegend" runat="server" Text="Daftar Users"></asp:Label>
            </legend>
            <telerik:RadGrid ID="GridDaftar" runat="server" GridLines="None" AllowSorting="True"
                OnNeedDataSource="GridDaftar_NeedDataSource" OnItemCommand="GridDaftar_ItemCommand"
                AllowPaging="true" ShowStatusBar="true" Skin="Black">
                <ClientSettings AllowGroupExpandCollapse="true" EnablePostBackOnRowClick="false">
                </ClientSettings>
                <MasterTableView AutoGenerateColumns="false" Width="100%" CommandItemDisplay="Top" DataKeyNames="user_id">
                    <Columns>
                        <telerik:GridBoundColumn DataField="user_name" HeaderText="Nama">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="last_login" HeaderText="Login terakhir">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="last_logout" HeaderText="Logout terakhir">
                        </telerik:GridBoundColumn>
                        <telerik:GridButtonColumn ButtonCssClass="btn btn-link" Text="Edit" CommandName="Edit">
                        </telerik:GridButtonColumn>
                        <telerik:GridButtonColumn ButtonCssClass="btn btn-danger" Text="Delete" CommandName="Delete">
                            <ItemStyle ForeColor="White" />
                        </telerik:GridButtonColumn>
                    </Columns>
                    <CommandItemSettings AddNewRecordText="Add Users" RefreshText="Refresh" ShowRefreshButton="true"
                        ShowAddNewRecordButton="true" />
                </MasterTableView>
                <PagerStyle Mode="NextPrevAndNumeric" />
            </telerik:RadGrid>
        </div>
    </div>
    
    <script src="../Scripts/Users.js"></script>
</asp:Content>

