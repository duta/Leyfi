﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Leyfi.Data.Interfaces;

namespace Leyfi.Data.MySQL
{
    public class Factory : IFactory
    {
        string _connString;

        public Factory(string connString)
        {
            _connString = connString;
        }

        public IUsers GetUsers()
        {
            return new Users(_connString);
        }

        public IRoles GetRoles()
        {
            return new Roles(_connString);
        }

        public IElements GetElements()
        {
            return new Elements(_connString);
        }
    }
}
