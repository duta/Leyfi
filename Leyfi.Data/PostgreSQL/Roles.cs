﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBProvider;
using Npgsql;
using Leyfi.Data.Interfaces;

namespace Leyfi.Data.PostgreSQL
{
    internal class Roles : IRoles
    {
         DBProvider.PostgreSQL _ps;

        public Roles(string connStr)
        {
            _ps = new DBProvider.PostgreSQL(connStr);
        }

        public List<Entities.Roles> GetAll()
        {
            StringBuilder sb = new StringBuilder();
            List<Entities.Roles> result = new List<Entities.Roles>();
            sb.Append(" select ");
            sb.Append(" 	role_id,role_name,role_description  ");
            sb.Append(" from moresecure.roles  ");

            _ps.OpenConnectionAndCreateCommand();
            NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString());
            Entities.Roles na;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    na = new Entities.Roles();
                    na.role_id = int.Parse(reader["role_id"].ToString());
                    na.role_name = reader["role_name"].ToString();
                    na.role_description = reader["role_description"].ToString();
                    result.Add(na);
                }
            }
            reader.Close();
            _ps.CloseConnection();

            return result;
        }
        
        public int Upsert(int? role_id, string role_name, string role_description)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            if (role_id.HasValue)
            {
                sb.Append("update moresecure.roles set role_name= :valrolename, role_description= :valroledescription ");
                sb.Append("    where role_id = :valroleid");
            }
            else
            {
                sb.Append("insert into moresecure.roles (role_name, role_description) ");
                sb.Append(" values (:valrolename, :valroledescription) ");
            }

            NpgsqlParameter paramId, paramName, paramDesc;
            paramId = new NpgsqlParameter("valroleid", NpgsqlTypes.NpgsqlDbType.Integer);
            paramName = new NpgsqlParameter("valrolename", NpgsqlTypes.NpgsqlDbType.Text);
            paramDesc = new NpgsqlParameter("valroledescription", NpgsqlTypes.NpgsqlDbType.Text);

            NpgsqlParameter[] Params;
            if (role_id.HasValue)
            {
                Params = new NpgsqlParameter[] { paramName,paramDesc, paramId };
                paramId.Value = role_id.Value;               
            }
            else
            {
                Params = new NpgsqlParameter[] { paramName, paramDesc };
            }
            paramName.Value = role_name;
            paramDesc.Value = role_description;

            _ps.OpenConnectionAndCreateCommand();
            returnValue = _ps.ExecuteNonQuery(sb.ToString(), Params);
            return returnValue;
        }

        public int Delete(int role_id)
        {
            int returnValue = 0;
            StringBuilder sb;
            NpgsqlTransaction trans = null;

            try
            {
                _ps.OpenConnectionAndCreateCommand();
                using (trans = _ps.Comm.Connection.BeginTransaction())
                {
                    using (_ps.Comm)
                    {
                        _ps.Comm.Transaction = trans;
                       
                        sb = new StringBuilder();
                        sb.Append("delete from  moresecure.permissions ");
                        sb.Append(" where role_id = :valroleid  ");

                        NpgsqlParameter paramRoleId;
                        paramRoleId = new NpgsqlParameter("valroleid", NpgsqlTypes.NpgsqlDbType.Integer);

                        NpgsqlParameter[] Params = new NpgsqlParameter[] { paramRoleId };
                        paramRoleId.Value = role_id;
                        _ps.Comm.Parameters.AddRange(Params);
                        _ps.Comm.CommandText = sb.ToString();
                        returnValue += _ps.Comm.ExecuteNonQuery();

                        sb = new StringBuilder();
                        sb.Append("delete from  moresecure.usersinroles ");
                        sb.Append(" where role_id = :valroleid  ");
                        paramRoleId.Value = role_id;
                        _ps.Comm.Parameters.AddRange(Params);
                        _ps.Comm.CommandText = sb.ToString();
                        returnValue += _ps.Comm.ExecuteNonQuery();

                        sb = new StringBuilder();
                        sb.Append(" delete from  moresecure.roles ");
                        sb.Append(" where role_id = :valroleid ");
                        paramRoleId.Value = role_id;
                        _ps.Comm.Parameters.AddRange(Params);
                        _ps.Comm.CommandText = sb.ToString();
                        returnValue += _ps.Comm.ExecuteNonQuery();

                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                if (trans != null) trans.Rollback();
            }
            finally
            {
                _ps.CloseConnection();
            }
          
            return returnValue;
        }
        
        public List<Entities.Permissions> GetPermissions()
        {
            StringBuilder sb = new StringBuilder();
            List<Entities.Permissions> result = new List<Entities.Permissions>();
            sb.Append(QueryGetPermissions());
            _ps.OpenConnectionAndCreateCommand();
            NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString());
            Entities.Permissions na;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    na = new Entities.Permissions();
                    na.roles = new Entities.Roles();
                    na.elements = new Entities.Elements();

                    na.roles.role_id = int.Parse(reader["role_id"].ToString());
                    na.roles.role_name = reader["role_name"].ToString();
                    na.roles.role_description = reader["role_description"].ToString();

                    na.elements.element_id = string.IsNullOrEmpty(reader["element_id"].ToString()) ? new int?() : int.Parse(reader["element_id"].ToString());
                    na.elements.element_type = reader["element_type"].ToString();
                    result.Add(na);
                }
            }
            reader.Close();
            _ps.CloseConnection();

            return result;
        }

        public System.Data.DataTable GetPermissionsAsDatatable()
        {
            StringBuilder sb = new StringBuilder();
            List<Entities.Permissions> result = new List<Entities.Permissions>();
            sb.Append(QueryGetPermissions());
            _ps.OpenConnectionAndCreateCommand();
            return _ps.ExecuteDataTable(sb.ToString());
        }

        private string QueryGetPermissions() {
            StringBuilder sb = new StringBuilder();            
            sb.Append(" select  ");
            sb.Append("     r.*, e.element_id,e.element_type ");
            sb.Append(" from moresecure.roles r left join moresecure.permissions p on r.role_id = p.role_id ");
            sb.Append("             left join moresecure.elements e on p.element_id = e.element_id ");
            return sb.ToString();
        }
        
        public List<Entities.Elements> GetElementsPermittedByRoleID(int role_id)
        {
            StringBuilder sb = new StringBuilder();
            List<Entities.Elements> result = new List<Entities.Elements>();
            sb.Append(" select   ");
            sb.Append("     e.element_id,e.element_type  ");
            sb.Append(" from moresecure.roles r join moresecure.permissions p on r.role_id = p.role_id  ");
            sb.Append("   join moresecure.elements e on p.element_id = e.element_id  ");
            sb.Append(" where r.role_id = :valroleid ");

            NpgsqlParameter paramId;
            paramId = new NpgsqlParameter("valroleid", NpgsqlTypes.NpgsqlDbType.Integer);
            NpgsqlParameter[] Params= new NpgsqlParameter[] { paramId };
            paramId.Value = role_id;   

            _ps.OpenConnectionAndCreateCommand();
            NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString(), Params);
            Entities.Elements na;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    na = new Entities.Elements();                 
                    na.element_id = string.IsNullOrEmpty(reader["element_id"].ToString()) ? new int?() : int.Parse(reader["element_id"].ToString());
                    na.element_type = reader["element_type"].ToString();
                    result.Add(na);
                }
            }
            reader.Close();
            _ps.CloseConnection();
            return result;
        }
        
        public int InsertPermission(int role_id, int element_id)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append("insert into moresecure.permissions (role_id, element_id) ");
            sb.Append(" values (:valroleid, :valelementid) ");           

            NpgsqlParameter paramRoleId, paramElementID;
            paramRoleId = new NpgsqlParameter("valroleid", NpgsqlTypes.NpgsqlDbType.Integer);
            paramElementID = new NpgsqlParameter("valelementid", NpgsqlTypes.NpgsqlDbType.Integer);

            NpgsqlParameter[] Params;
            Params = new NpgsqlParameter[] { paramRoleId, paramElementID };
            paramRoleId.Value = role_id;
            paramElementID.Value = element_id;

            _ps.OpenConnectionAndCreateCommand();
            returnValue = _ps.ExecuteNonQuery(sb.ToString(), Params);
            return returnValue;
        }

        public bool ThisRoleHavePermissionFor(int role_id, int element_id)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select count(*) jml ");
            sb.Append(" from  moresecure.permissions ");
            sb.Append(" where role_id = :valroleid and element_id = :valelementid ");
            NpgsqlParameter paramRoleId, paramElementID;
            paramRoleId = new NpgsqlParameter("valroleid", NpgsqlTypes.NpgsqlDbType.Integer);
            paramElementID = new NpgsqlParameter("valelementid", NpgsqlTypes.NpgsqlDbType.Integer);

            NpgsqlParameter[] Params;
            Params = new NpgsqlParameter[] { paramRoleId, paramElementID };
            paramRoleId.Value = role_id;
            paramElementID.Value = element_id;

            _ps.OpenConnectionAndCreateCommand();
            string result = _ps.ExecuteScalar(sb.ToString(), Params);
            return (result != "0");
        }

        public List<Entities.Elements> AvailableElementsForRoleID(int role_id)
        {
            StringBuilder sb = new StringBuilder();
            List<Entities.Elements> result = new List<Entities.Elements>();
            sb.Append(" select element_id, element_type  ");
            sb.Append(" from moresecure.elements ");
            sb.Append(" where element_id not in ");
            sb.Append(" ( ");
            sb.Append(" select   ");
            sb.Append(" 	e.element_id ");
            sb.Append(" from moresecure.roles r join moresecure.permissions p on r.role_id = p.role_id  ");
            sb.Append("   join moresecure.elements e on p.element_id = e.element_id  ");
            sb.Append(" where r.role_id = :valroleid ");
            sb.Append(" ) ");
            NpgsqlParameter paramId;
            paramId = new NpgsqlParameter("valroleid", NpgsqlTypes.NpgsqlDbType.Integer);
            NpgsqlParameter[] Params = new NpgsqlParameter[] { paramId };
            paramId.Value = role_id;

            _ps.OpenConnectionAndCreateCommand();
            NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString(), Params);
            Entities.Elements na;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    na = new Entities.Elements();
                    na.element_id = string.IsNullOrEmpty(reader["element_id"].ToString()) ? new int?() : int.Parse(reader["element_id"].ToString());
                    na.element_type = reader["element_type"].ToString();
                    result.Add(na);
                }
            }
            reader.Close();
            _ps.CloseConnection();
            return result;
        }

        public int DeletePermission(int role_id, int element_id)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append("delete from  moresecure.permissions ");
            sb.Append(" where role_id = :valroleid and element_id = :valelementid ");           

            NpgsqlParameter paramRoleId, paramElementID;
            paramRoleId = new NpgsqlParameter("valroleid", NpgsqlTypes.NpgsqlDbType.Integer);
            paramElementID = new NpgsqlParameter("valelementid", NpgsqlTypes.NpgsqlDbType.Integer);

            NpgsqlParameter[] Params;
            Params = new NpgsqlParameter[] { paramRoleId, paramElementID };
            paramRoleId.Value = role_id;
            paramElementID.Value = element_id;

            _ps.OpenConnectionAndCreateCommand();
            returnValue = _ps.ExecuteNonQuery(sb.ToString(), Params);
            return returnValue;
        }

        public List<Entities.Users> UsersHaveThisRole(int role_id)
        {
            StringBuilder sb = new StringBuilder();
            List<Entities.Users> result = new List<Entities.Users>();
            sb.Append(" select uir.user_id, u.user_name ");
            sb.Append(" from moresecure.roles r join moresecure.usersinroles uir on r.role_id = uir.role_id ");
            sb.Append("                         join moresecure.users u on uir.user_id = u.user_id ");
            sb.Append(" where r.role_id = :valroleid ");
            NpgsqlParameter paramRoleId;
            paramRoleId = new NpgsqlParameter("valroleid", NpgsqlTypes.NpgsqlDbType.Integer);

            NpgsqlParameter[] Params;
            Params = new NpgsqlParameter[] { paramRoleId };
            paramRoleId.Value = role_id;

            _ps.OpenConnectionAndCreateCommand();
            NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString(), Params);
            Entities.Users na;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    na = new Entities.Users();
                    na.user_id =  int.Parse(reader["user_id"].ToString());
                    na.user_name = reader["user_name"].ToString();
                    result.Add(na);
                }
            }
            reader.Close();
            _ps.CloseConnection();
            return result;
        }
    }
}
