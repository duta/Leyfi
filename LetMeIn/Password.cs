﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Linq;
using System.Text;
using System.Configuration;
using Microsoft.VisualBasic;

namespace LetMeIn
{
    [DataContract]   
    public class DigDown {
        [DataMember]
        public string aivi;
        [DataMember]        
        public string sesi;
        [DataMember]        
        public string content;
    } 

   public class Password
    {
       private RSACryptoServiceProvider rsaKey;
       public Password() {
          rsaKey = new RSACryptoServiceProvider();
           rsaKey.FromXmlString("<RSAKeyValue><Modulus>oYotW4NhkFGFAaBXUybLuls4agd6bGc82bgePpvG4Yhgbd8BTFqv+E78qKcFI6pZ4HgckjT7FMpg2LO7b6lHWoAeO6AVb3yHydsChIo3oBF9Lrmn8LPkMHebYxatgW59ANgfPTKMOtxp57VuTpMIj51wUpahk5xd8WneDhRl0eE=</Modulus><Exponent>AQAB</Exponent><P>1T1+WGH4GAzssLb7mAspknGMkyaqH6rA/D8KtDm/2Qc/Xd4JdtKpfrJcHjSHpoYpTBIGBN/N8xhF11WKR7TkWw==</P><Q>we6vMuGq1jNDNrNLAq+pnEe+Uk38OMUrcEPvFqcwCPQuE+fIgcbRyN9omIrMTkF7WtGUxsKVTgFuCJbc+cpHcw==</Q><DP>nVWg+k9WwVAP95cbbaSWyNF1GkHF9lWDzNoq6K1Pay9YxsaEDyMCIlQxeavihrComdMWk9QZ3gqDxYz82UmTzw==</DP><DQ>TkQWvtEmUhYULTJEtovQRrsJi0vf28VvF3rDNgA5OZPaOLxVRlLH/Wp0WwPBGmAA+ZEVmvWSrg2Tq7G+8qx5Ew==</DQ><InverseQ>qNf/9idbQeeA7yYxGvP5ufFZOv2BXJUiFKkI7/jhIdRV7J0cIqEJMPfIcB+fP/xBYGLeEMTa1qJZ/e8O+J2xjA==</InverseQ><D>IHAGu6GeLcSw2KrB3kikED/RDDmEM9BnP7DhIeFWBmPEOUvWjuaG/22c/qDLP2Yttg1ob1OEN6CL4ff1Y26yjP/52yoDFvhbGGb54zX78Yh3nYA27G+A4Uj5FevKoE9+lYKv4gA45o+U9zH9IQY2MvhfOWjh5WvuQA07ISyodCE=</D></RSAKeyValue>");
       }
       
       public byte[] Encrypt(string content)
       {   
           string passToSave; 
           byte[] encryptedMessage;
           using (Aes aes = new AesCryptoServiceProvider())
           {
               byte[] iv = aes.IV;
               //using (RSACryptoServiceProvider rsaKeyNow = new RSACryptoServiceProvider())
               //{
                   rsaKey.ImportCspBlob(rsaKey.ExportCspBlob(false));
                   // Encrypt the session key
                   RSAOAEPKeyExchangeFormatter keyFormatter = new RSAOAEPKeyExchangeFormatter(rsaKey);
                   byte[] encryptedSessionKey = keyFormatter.CreateKeyExchange(aes.Key, typeof(Aes));

                   // Encrypt the message
                   using (MemoryStream ciphertext = new MemoryStream())
                   using (CryptoStream cs = new CryptoStream(ciphertext, aes.CreateEncryptor(), CryptoStreamMode.Write))
                   {
                       byte[] plaintextMessage = Encoding.UTF8.GetBytes(content);
                       cs.Write(plaintextMessage, 0, plaintextMessage.Length);
                       cs.Close();

                       encryptedMessage = ciphertext.ToArray();
                   }
                   DigDown dig = new DigDown();
                   dig.aivi = Convert.ToBase64String(iv);
                   dig.sesi = Convert.ToBase64String(encryptedSessionKey);
                   dig.content = Convert.ToBase64String(encryptedMessage);
                   MemoryStream stream1 = new MemoryStream();
                   DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(DigDown));
                   ser.WriteObject(stream1, dig);
                   string jsonString = Encoding.UTF8.GetString(stream1.ToArray());
                   stream1.Close();
                   passToSave = Leyfi.LetMeIn.MD5.Encrypt(jsonString, "&%#@?,:*");
               //}
               string encryptedpassword = Convert.ToBase64String(Encoding.UTF8.GetBytes(passToSave));
               return Convert.FromBase64String(encryptedpassword);
           }
       }
       private string Decrypt(byte[] content)
       {
           string savedPassword = Leyfi.LetMeIn.MD5.Decrypt(Encoding.UTF8.GetString(content, 0, content.Length), "&%#@?,:*");
           MemoryStream stream1 = new MemoryStream(Encoding.UTF8.GetBytes(savedPassword));
           DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(DigDown));
           DigDown dig = (DigDown)ser.ReadObject(stream1);
           stream1.Close();

           byte[] iv = Convert.FromBase64String(dig.aivi);
           byte[] encryptedSessionKey = Convert.FromBase64String(dig.sesi);
           byte[] encryptedMessage = Convert.FromBase64String(dig.content);
         
           using (Aes aes = new AesCryptoServiceProvider())
           {
               aes.IV = iv;

               // Decrypt the session key
               RSAOAEPKeyExchangeDeformatter keyDeformatter = new RSAOAEPKeyExchangeDeformatter(rsaKey);
               aes.Key = keyDeformatter.DecryptKeyExchange(encryptedSessionKey);

               // Decrypt the message
               using (MemoryStream plaintext = new MemoryStream())
               using (CryptoStream cs = new CryptoStream(plaintext, aes.CreateDecryptor(), CryptoStreamMode.Write))
               {
                   cs.Write(encryptedMessage, 0, encryptedMessage.Length);
                   cs.Close();

                   string message = Encoding.UTF8.GetString(plaintext.ToArray());
                   return message;
               }
           }
       }

       public bool Pass(string inputPassword, byte[] savedPassword) 
       {
           return string.Compare(inputPassword, Decrypt(savedPassword)) == 0 ? true : false;
       }



    }
}
