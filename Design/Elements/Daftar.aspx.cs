﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Leyfi;
using Leyfi.Data.Entities;

public partial class Elements_Daftar : BasePage
{
   
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["EditOpElements"] = null;
        if (!Page.IsPostBack) {            
            GridDaftar.Rebind();
        }       
    }

    protected void GridDaftar_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "Delete") {
            CreateFactory().GetElements().Delete(((Elements)e.Item.DataItem).element_id.Value);
            GridDaftar_NeedDataSource(null, null);
        }
        else if (e.CommandName == "Edit") {
            Session["EditOpElements"] = ((Elements)e.Item.DataItem);
            Response.Redirect("TambahBaru.aspx");
        }
        else if (e.CommandName == "InitInsert") {
            Response.Redirect("TambahBaru.aspx");
        }
        else if (e.CommandName == "RebindGrid") {
            GridDaftar.Rebind();
        }
    }
       
    protected void GridDaftar_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        List<Leyfi.Data.Entities.Elements> elements = CreateFactory().GetElements().GetAll();
        GridDaftar.DataSource = elements;
    }
}