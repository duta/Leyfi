﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Leyfi.Data.Entities
{
    public class Users
    {
        public int? user_id { get; set; }
        public string user_name { get; set; }
        public byte[] password { get; set; }
        public DateTime? last_login { get; set; }
        public DateTime? last_logout { get; set; }
    }

    public class Roles
    {
        public int? role_id { get; set; }
        public string role_name { get; set; }
        public string role_description { get; set; }
    }

    public class Elements {
        public int? element_id { get; set; }
        public string element_type { get; set; }
    }

    public class UsersInRoles {
        public Users users { get; set; }
        public Roles roles { get; set; }
    }

    public class Permissions {
        public Roles roles { get; set; }
        public Elements elements { get; set; }
    }
}
