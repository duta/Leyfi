﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using MySql.Data.MySqlClient;
using Leyfi.Data.Interfaces;

namespace Leyfi.Data.MySQL
{
    internal class Users : IUsers
    {
        MySqlConnection _ps;

        public Users(string connStr)
        {
            _ps = new MySqlConnection(connStr);
        }

        public List<Entities.Users> GetAll()
        {
            StringBuilder sb = new StringBuilder();
            List<Entities.Users> result = new List<Entities.Users>();
            sb.Append(" select ");
            sb.Append(" 	user_id,user_name, DATE_FORMAT(last_login, '%d-%m-%Y %H:%i:%s') last_login, DATE_FORMAT(last_logout, '%d-%m-%Y %H:%i:%s') last_logout ");
            sb.Append(" from users  ");

            _ps.Open();

            MySqlCommand cmd = new MySqlCommand(sb.ToString(), _ps);
            var reader = cmd.ExecuteReader();
            Entities.Users na;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    na = new Entities.Users();
                    na.user_id = int.Parse(reader["user_id"].ToString());
                    na.user_name = reader["user_name"].ToString();
                    na.last_login = string.IsNullOrEmpty(reader["last_login"].ToString()) ? new DateTime?() : DateTime.ParseExact(reader["last_login"].ToString(), "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    na.last_logout = string.IsNullOrEmpty(reader["last_logout"].ToString()) ? new DateTime?() : DateTime.ParseExact(reader["last_logout"].ToString(), "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);                  
                    result.Add(na);
                }
            }
            reader.Close();
            _ps.Close();

            return result;
        }


        public List<Entities.UsersInRoles> GetUserInRoles(string user_name)
        {
            StringBuilder sb = new StringBuilder();
            List<Entities.UsersInRoles> result = new List<Entities.UsersInRoles>();
            //sb.Append(" select ");
            //sb.Append(" 	user_id,user_name, to_char(last_activity, 'dd-mm-yyyy HH24:mi:ss') last_activity ");
            //sb.Append(" from users  ");

            //_ps.OpenConnectionAndCreateCommand();
            //NpgsqlDataReader reader = _ps.ExecuteDataReader(sb.ToString());
            Entities.UsersInRoles na;
            //if (reader.HasRows)
            {
            //    while (reader.Read())
                {
                    //na = new Entities.Users();
                    //na.user_id = int.Parse(reader["user_id"].ToString());
                    //na.user_name = reader["user_name"].ToString();
                    //na.last_activity = string.IsNullOrEmpty(reader["last_activity"].ToString()) ? new DateTime?() : DateTime.ParseExact(reader["last_activity"].ToString(), "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    //result.Add(na);
                }
            }
            //reader.Close();
            //_ps.CloseConnection();

            return result;
        }


        public int Delete(int user_id)
        {
            int returnValue = 0;
            StringBuilder sb;
            MySqlTransaction trans = null;
            MySqlCommand cmd = null;
            try
            {
                _ps.Open();

                using (trans = _ps.BeginTransaction())
                {

                    sb = new StringBuilder();
                    sb.Append("delete from  usersinroles ");
                    sb.Append(" where user_id = @valuserid  ");
                    cmd = new MySqlCommand(sb.ToString(), _ps, trans);
                    cmd.Parameters.AddWithValue("@valuserid", user_id);
                    returnValue += cmd.ExecuteNonQuery();

                    sb = new StringBuilder();
                    sb.Append(" delete from  users ");
                    sb.Append(" where user_id = :valuserid ");
                    cmd = new MySqlCommand(sb.ToString(), _ps, trans);
                    cmd.Parameters.AddWithValue("@valuserid", user_id);
                    returnValue += cmd.ExecuteNonQuery();

                    trans.Commit();
                }
            }
            catch (Exception ex)
            {
                if (trans != null) trans.Rollback();
            }
            finally
            {
                _ps.Close();
            }

            return returnValue;
        }

        public int InsertUsersInRoles(int role_id, int user_id)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append("insert into usersinroles (role_id, user_id) ");
            sb.Append(" values (@valroleid, @valuserid) ");
            _ps.Open();

            MySqlCommand cmd = new MySqlCommand(sb.ToString(), _ps);
            cmd.Parameters.AddWithValue("@valroleid", role_id);
            cmd.Parameters.AddWithValue("@valuserid", user_id);
            returnValue = cmd.ExecuteNonQuery();

            _ps.Close();
            return returnValue;
        }

        public int DeleteUsersInRoles(int role_id, int user_id)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append("delete from  usersinroles ");
            sb.Append(" where role_id = @valroleid and user_id = @valuserid ");

            _ps.Open();

            MySqlCommand cmd = new MySqlCommand(sb.ToString(), _ps);
            cmd.Parameters.AddWithValue("@valroleid", role_id);
            cmd.Parameters.AddWithValue("@valuserid", user_id);
            returnValue = cmd.ExecuteNonQuery();

            _ps.Close();
            return returnValue;
        }

        public int Upsert(int? user_id, string user_name, byte[] password)
        {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();
            if (user_id.HasValue)
            {
                sb.Append("update users set user_name= @valusername, password = @valpassword ");
                sb.Append("    where user_id = @valuserid");
            }
            else
            {
                sb.Append("insert into users (user_name, password) ");
                sb.Append(" values (@valusername, @valpassword ) ");
            }
            _ps.Open();

            MySqlCommand cmd = new MySqlCommand(sb.ToString(), _ps);
            if (user_id.HasValue)
            {
                cmd.Parameters.AddWithValue("@valuserid", user_id.Value);
            }
            cmd.Parameters.AddWithValue("@valusername", user_name);
            cmd.Parameters.AddWithValue("@valpassword", password);
            returnValue = cmd.ExecuteNonQuery();

            _ps.Close();
            return returnValue;
        }

        public List<Entities.Roles> GetRolesByUserID(int user_id)
        {
            StringBuilder sb = new StringBuilder();            
            sb.Append(" select ");
            sb.Append("     r.* ");
            sb.Append(" from users u  join usersinroles uir on u.user_id = uir.user_id ");
            sb.Append("      join roles r on uir.role_id = r.role_id ");
            sb.Append(" where u.user_id = @valuserid  ");
            _ps.Open();

            MySqlCommand cmd = new MySqlCommand(sb.ToString(), _ps);
            cmd.Parameters.AddWithValue("@valuserid", user_id);
            var reader = cmd.ExecuteReader();
            Entities.Roles na;
            List<Entities.Roles> result = new List<Entities.Roles>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    na = new Entities.Roles();
                    na.role_id = int.Parse(reader["role_id"].ToString());
                    na.role_name = reader["role_name"].ToString();
                    na.role_description = reader["role_description"].ToString();
                    result.Add(na);
                }
            }
            reader.Close();
            _ps.Close();

            return result;

        }


        public List<Entities.Roles> AvailableRolesForUserID(int user_id)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select  ");
            sb.Append("   r.*	 ");
            sb.Append(" from roles r ");
            sb.Append(" where  ");
            sb.Append("   r.role_id not in ( ");
            sb.Append(" 	select ");
            sb.Append(" 		r.role_id ");
            sb.Append(" 	from users u join usersinroles uir on u.user_id = uir.user_id ");
            sb.Append("          join roles r on uir.role_id = r.role_id ");
            sb.Append("     where u.user_id = @valuserid ");
            sb.Append(" ) ");
            _ps.Open();

            MySqlCommand cmd = new MySqlCommand(sb.ToString(), _ps);
            cmd.Parameters.AddWithValue("@valuserid", user_id);
            var reader = cmd.ExecuteReader();
            Entities.Roles na;
            List<Entities.Roles> result = new List<Entities.Roles>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    na = new Entities.Roles();
                    na.role_id = int.Parse(reader["role_id"].ToString());
                    na.role_name = reader["role_name"].ToString();
                    na.role_description = reader["role_description"].ToString();
                    result.Add(na);
                }
            }
            reader.Close();
            _ps.Close();

            return result;
        }


        public bool LastLoginExpired(int user_id, int max_hour_expired)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select  DATE_FORMAT(last_login, '%d-%m-%Y %H:%i:%s')  last_login ");
            sb.Append(" from  users ");
            sb.Append(" where user_id = @valuserid  ");
            _ps.Open();

            MySqlCommand cmd = new MySqlCommand(sb.ToString(), _ps);
            cmd.Parameters.AddWithValue("@valuserid", user_id);
            var result = cmd.ExecuteScalar();

            _ps.Close();
            if (result == null || string.IsNullOrEmpty(result.ToString())) return true;
            DateTime resultDate = DateTime.ParseExact(result.ToString(), "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            TimeSpan span = DateTime.Now.Subtract(resultDate);
            return span.TotalHours > max_hour_expired;
        }


        public bool LastLogoutNull(int user_id)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select  DATE_FORMAT(last_logout, '%d-%m-%Y %H:%i:%s')  last_logout ");
            sb.Append(" from  users ");
            sb.Append(" where user_id = @valuserid  ");
            _ps.Open();

            MySqlCommand cmd = new MySqlCommand(sb.ToString(), _ps);
            cmd.Parameters.AddWithValue("@valuserid", user_id);
            var result = cmd.ExecuteScalar();

            _ps.Close();
            return result == null || string.IsNullOrEmpty(result.ToString());
        }

        public int SetUserLogOut(int user_id)
        {
            return SetUserLog(user_id, false);
        }

        public int SetUserLogIn(int user_id)
        {
            return SetUserLog(user_id, true);
        }

        private int SetUserLog(int user_id, bool isLogin) {
            int returnValue = 0;
            StringBuilder sb = new StringBuilder();

            sb.Append("update users set ");
            if (isLogin)
            {
                sb.Append(" last_login = now(), last_logout = NULL ");
            }
            else {
                sb.Append(" last_logout= now() ");                
            }
            sb.Append("    where user_id = @valuserid");

            _ps.Open();

            MySqlCommand cmd = new MySqlCommand(sb.ToString(), _ps);
            cmd.Parameters.AddWithValue("@valuserid", user_id);
            returnValue = cmd.ExecuteNonQuery();

            _ps.Close();
            return returnValue;
        }

        public int? GetUserIDByUserName(string user_name) {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select  user_id ");
            sb.Append(" from  users ");
            sb.Append(" where upper(user_name) = @valusername  ");
            _ps.Open();

            MySqlCommand cmd = new MySqlCommand(sb.ToString(), _ps);
            cmd.Parameters.AddWithValue("@valusername", user_name);
            var result = cmd.ExecuteScalar().ToString();

            _ps.Close();
            if (result == null || string.IsNullOrEmpty(result.ToString())) return new Nullable<int>();
            return int.Parse(result);
        }

        public byte[] GetPasswordByUserId(int user_id) {
            byte[] result = {};
            StringBuilder sb = new StringBuilder();
            sb.Append(" select ");
            sb.Append(" 	password ");
            sb.Append(" from users  ");
            sb.Append(" where user_id = @valuserid  ");

            _ps.Open();

            MySqlCommand cmd = new MySqlCommand(sb.ToString(), _ps);
            cmd.Parameters.AddWithValue("@valuserid", user_id);
            var reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    if (reader[0] != DBNull.Value)
                    {
                        result = (byte[])reader[0];
                    }
                }
            }
            reader.Close();
            _ps.Close();
            return result;
        }


        public Entities.Users GetUserByUserID(int user_id)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select ");
            sb.Append(" 	user_id,user_name, DATE_FORMAT(last_login, '%d-%m-%Y %H:%i:%s') last_login, DATE_FORMAT(last_logout, '%d-%m-%Y %H:%i:%s') last_logout, password ");
            sb.Append(" from users  ");
            sb.Append(" where user_id = @valuserid ");
            _ps.Open();

            MySqlCommand cmd = new MySqlCommand(sb.ToString(), _ps);
            cmd.Parameters.AddWithValue("@valuserid", user_id);
            var reader = cmd.ExecuteReader();

            Entities.Users na = new Entities.Users();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    na = new Entities.Users();
                    na.user_id = int.Parse(reader["user_id"].ToString());
                    na.user_name = reader["user_name"].ToString();
                    na.last_login = string.IsNullOrEmpty(reader["last_login"].ToString()) ? new DateTime?() : DateTime.ParseExact(reader["last_login"].ToString(), "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    na.last_logout = string.IsNullOrEmpty(reader["last_logout"].ToString()) ? new DateTime?() : DateTime.ParseExact(reader["last_logout"].ToString(), "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    na.password = (byte[])reader["password"];
                }
            }
            reader.Close();
            _ps.Close();

            return na;
        }
    }
}
