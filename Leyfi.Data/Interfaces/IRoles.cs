﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Leyfi.Data.Entities;

namespace Leyfi.Data.Interfaces
{
    public interface IRoles
    {        
        int Upsert(int? role_id, string role_name, string role_description);
        int Delete(int role_id);
        int InsertPermission(int role_id, int element_id);
        int DeletePermission(int role_id, int element_id);
        bool ThisRoleHavePermissionFor(int role_id, int element_id);
        List<Users> UsersHaveThisRole(int role_id);
        List<Roles> GetAll();
        List<Permissions> GetPermissions();
        List<Elements> GetElementsPermittedByRoleID(int role_id);
        DataTable GetPermissionsAsDatatable();
        List<Elements> AvailableElementsForRoleID(int role_id);
    }
}
